/*
** main.c for my_select in /home/sebastien/travaux/my_select
**
** Made by sebastien
** Login   <sebastien@epitech.net>
**
** Started on  Tue Jan 14 17:15:32 2014 sebastien
** Last update Wed Nov 12 15:27:35 2014 de-dum_m
*/

#include <string.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdlib.h>
#include "my_select.h"


static char	*combine_chars(char *s1, char *s2)
{
  char		*res;

  if (!(res = malloc(strlen(s1) + strlen(s2) + 2)))
    return (NULL);
  res = memset(res, 0, strlen(s1) + strlen(s2) + 2);
  res = strcat(res, s1);
  res = strcat(res, " ");
  res = strcat(res, s2);
  return (res);
}

static int	get_arg(int argc, char **argv, t_list **root)
{
  t_list	*tmp;
  int		i;

  i = 1;
  if (argc == 1)
    return (usage());
  if ((init_roots(root)) == -1)
    return (-1);
  tmp = *root;
  while (argv[i] && argv[i + 1])
  {
    if ((push_back(&tmp, combine_chars(argv[i], argv[i + 1]))) == -1)
      return (-1);
    i += 2;
  }
  return (0);
}

int			main(int argc, char **argv, char **env)
{
  struct termios	term_attr;

  root_args = NULL;
  if ((fd_tty = open("/dev/tty", O_RDWR)) == -1)
    return (puterror("error: could not open /dev/tty\n"));
  if (get_arg(argc, argv, &root_args) == -1)
    return (-1);
  if ((init_term(env, &term_attr)) == -1)
    return (-1);
  signal(SIGWINCH, get_sigwinch);
  if ((get_cmd()) == -1)
    return (-1);
  restore_term(&term_attr);
  disp_select(root_args);
  free_list(root_args);
  return (0);
}
