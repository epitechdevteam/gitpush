# Interactive git script

Displays a nice list to select which files to add to a commit. Can commit and push as well.

## Installation

```
git clone git@bitbucket.org:epitechdevteam/gitpush.git
cd gitpush
make -C select_files
ln -s $(pwd)/git-add-commit.sh ~/bin # Or another PATH included directory
```
Edit MY_SELECT= in git-add-commit.sh (line 3) to absolute path of ./my_select

## Usage

Just execute the damn thing. In a git directory obviously.
Chose what files to add/delete, a commit message, and if you want, a branch to push to.

## Limitations

Does not work with evil filenames that include whitspaces.

###### Thanks to [seb](http://github.com/sebastiencs) for his selection tool.